<?php
/**
 * @file
 * feeds_twitter.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function feeds_twitter_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'tweets';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Tweets';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '#lvdug Tweets';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_twitter_avatar' => 'field_twitter_avatar',
    'body' => 'body',
  );
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Field: Content: Twitter Avatar Text */
  $handler->display->display_options['fields']['field_twitter_avatar_text']['id'] = 'field_twitter_avatar_text';
  $handler->display->display_options['fields']['field_twitter_avatar_text']['table'] = 'field_data_field_twitter_avatar_text';
  $handler->display->display_options['fields']['field_twitter_avatar_text']['field'] = 'field_twitter_avatar_text';
  $handler->display->display_options['fields']['field_twitter_avatar_text']['label'] = '';
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_twitter_avatar_text']['type'] = 'custom_formatters_twitter_avatar_text';
  $handler->display->display_options['fields']['field_twitter_avatar_text']['field_api_classes'] = 0;
  /* Field: Content: Twitter Username */
  $handler->display->display_options['fields']['field_twitter_username']['id'] = 'field_twitter_username';
  $handler->display->display_options['fields']['field_twitter_username']['table'] = 'field_data_field_twitter_username';
  $handler->display->display_options['fields']['field_twitter_username']['field'] = 'field_twitter_username';
  $handler->display->display_options['fields']['field_twitter_username']['label'] = '';
  $handler->display->display_options['fields']['field_twitter_username']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_twitter_username']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_twitter_username']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_twitter_username']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_twitter_username']['type'] = 'custom_formatters_twitter_username';
  $handler->display->display_options['fields']['field_twitter_username']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tweet_feed_item' => 'tweet_feed_item',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'tweets';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = '#lvdug Tweets';
  $handler->display->display_options['menu']['weight'] = '4';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $export['tweets'] = $view;

  return $export;
}
