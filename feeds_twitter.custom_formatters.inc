<?php
/**
 * @file
 * feeds_twitter.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function feeds_twitter_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'twitter_avatar_text';
  $formatter->label = 'Twitter Avatar Text';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '<img src="[node:field_twitter_avatar_text]" />';
  $export['twitter_avatar_text'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'twitter_username';
  $formatter->label = 'Twitter Username';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'text';
  $formatter->code = '$tuname = explode("@", $variables[\'#items\'][0][\'safe_value\']);
preg_match(\'#\\((.*?)\\)#\', $tuname[1], $rname);
print l(t($rname[1]), \'https://twitter.com/#!/\' . $tuname[0], array(\'attributes\' => array(\'target\' => \'_blank\'))) . \' \' . l(t(\'@\' . $tuname[0]), \'https://twitter.com/#!/\' . $tuname[0], array(\'attributes\' => array(\'target\' => \'_blank\')));';
  $export['twitter_username'] = $formatter;

  return $export;
}
