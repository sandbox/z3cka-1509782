<?php
/**
 * @file
 * feeds_twitter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feeds_twitter_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feeds_twitter_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feeds_twitter_node_info() {
  $items = array(
    'tweet_feed' => array(
      'name' => t('Tweet Feed'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'tweet_feed_item' => array(
      'name' => t('Tweet Feed Item'),
      'base' => 'node_content',
      'description' => t('These are tweets saved as local nodes via feeds from twitter.com rss imports.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
